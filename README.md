# VSPG Cli Linux

This is the CLI for VSPG if you're on Linux.
No need to know how Curl or Json works, just run the CLI.

You can use the default server (https://api.vspg.otbm.fr) or use your own.

## Installation

```console
git clone https://framagit.org/vspg/vspg_cli_bash.git
cd vspg_cli_bash
chmox +x vspg-cli
```
You can move the vspg-cli file where you want.


## Usage

### Configuration
For the first use, please run the *-c* option to generate the config file :
```console
./vspg-cli -c
```

The configuration file will be created in *~/.vspg/vspg.ini* .
You can edit the file to modify the default values according to the value in VSPG.

```console
[default]
port = 443
host = api.vspg.otbm.fr

number_of_words = 3
number_of_numbers = 5
separator = 1
language = fr
use_xclip = false

[security]
force_http = false
```

You can rebuild the configuration file with the *-r* (the old configuration file will be backuped):
```console
./vspg-cli -r
Renaming old ini file to /home/youruser/.vspg/vspg.ini-1589743152
ini file not found.
Creating '/home/youruser/.vspg/vspg.ini'

```

The configuration file will be created in *~/.vspg/vspg.ini* .

You can get the value by runnig the *-h* option
```console
Usage: ./vspg-cli [options]
-h : show this help
-c : write the configuration file in '/home/enky/.vspg/vspg.ini'
-v : show the versions
-u <host> : set a different VSPG API url [api.vspg.otbm.fr]
-w <number> : set the number of wolrd you want
-n <number> : set the number of digits you ll have in your number
-s <number> : set the separator type [0|1|2]
-l <language> : set the language you want [fr|fr_simple|en|it|es|de]
-m <number> : set the minimum length of the password
-i <file> : use an alternative config file [~/.vspg/alt-config.ini]
-a : get access_key
-r : rebuild the configuration file

Exemple:
./vspg-cli
exemple with arguments:
./vspg-cli -w 1 -n 5 -s 0 -l fr

For more information go to : https://vspg.otbm.fr
```

### Run
#### Get Password
Just run *./vspg-cli* (you will use the default value in the ini file)
```console
./vspg-cli 
AnkyloseraisDepetriezEchevellerai`11099
```

or add argument for a specific run *./vspg-cli -w 3 -l en* (3 words form the english dictionary)
```console
./vspg-cli -w 3 -l en 
CountermanUnregenerableExterminates_95802
```


If you install Xclip and set the *use_xclip* value to *true* the password will be copied directly in your Clipboard.

```console
./vspg-cli
The password is copied in your clipboard
```

#### Get Access Key

You can also get an Access And Secret key:

```console
./vspg-cli -a
A2ORHZQKJPVN2QHAO3VR
SFVwJnJQ8aWEJZBNFN7oKjULJpeoo1TYFUoTKJ10
```

### Check version

You can use the *-v* option to check if you have the latest version of the CLI.

```console
./vspg-cli -v
CLI Version : 0.4 (Latest version :0.4)
Server Version : 1.4 (https://api.vspg.otbm.fr:443)
```